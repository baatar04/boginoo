var admin = require('firebase-admin');

var serviceAccount = require('/home/baatarvan/Documents/Nest Academy/Projects/boginoo/functions/boginoo-23336-firebase-adminsdk-m0rti-ecb3f2ebae.json');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
});

admin
  .auth()
  .listUsers()
  .then(users => {
    admin
      .auth()
      .deleteUsers(users.users.map(({ uid }) => uid))
      .then(() => {
        console.log('Successfully deleted user');
      })
      .catch(error => {
        console.log('Error deleting user:', error);
      });
  });
