import classes from './urls.module.css';

const Urls = ({ shortUrl, enteredUrl }) => {
  const onClipboard = () => {
    navigator.clipboard.writeText(shortUrl);
  };

  return (
    <div>
      <p className={classes.title}>Өгөгдсөн холбоос:</p>
      <p className={classes.container}>{enteredUrl}</p>
      <p className={classes.title}>Богино холбоос:</p>
      <div className={classes.containerflex}>
        <span className={classes.url}>{shortUrl}</span>
        <span className={classes.copy} onClick={onClipboard}>
          Хуулж авах
        </span>
      </div>
    </div>
  );
};

export default Urls;
