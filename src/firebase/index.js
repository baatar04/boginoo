import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';

const firebaseConfig = {
  apiKey: 'AIzaSyDcM-XKLSQ85N5qVu4HuCXM2b7NfRXLtyg',
  authDomain: 'boginoo-23336.firebaseapp.com',
  projectId: 'boginoo-23336',
  storageBucket: 'boginoo-23336.appspot.com',
  messagingSenderId: '535495995462',
  appId: '1:535495995462:web:4187c202fe2c78cb58da47',
};

firebase.initializeApp(firebaseConfig);

export const auth = firebase.auth();
export const firestore = firebase.firestore();
export default firebase;
