import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { AuthProvider } from './Auth';

import Logo from './components/logo';
import Header from './components/header';
import Footer from './components/footer';
import Home from './pages/home';
import Login from './pages/login';
import Signup from './pages/signup';
import Forgot from './pages/forgot';
import Urlredirect from './urlredirect';

const App = () => {
  return (
    <AuthProvider>
      <Router>
        <div>
          <Header />
          <Logo />
          <Route path='/login' component={Login} />
          <Route path='/signup' component={Signup} />
          <Route path='/forgot' component={Forgot} />
          <Route path='/:firestoreId' component={Urlredirect} />
          <Route exact path='/' component={Home} />
          <Footer />
        </div>
      </Router>
    </AuthProvider>
  );
};

export default App;
