import { useState } from 'react';
import { auth } from '../firebase';

import classes from './forgot.module.css';

const Forgot = () => {
  const [enteredEmail, setEnteredEmail] = useState('');
  const [isSuccessful, setIsSuccessful] = useState(false);

  const resetPassword = async () => {
    try {
      await auth.sendPasswordResetEmail(enteredEmail);
      setIsSuccessful(true);
    } catch (err) {
      alert(err.message);
    }
  };

  if (isSuccessful) {
    return <p className={classes.successful}>Та цахим хаягаа шалгаарай</p>;
  }

  return (
    <div className={classes.forgot}>
      <h2 className={classes.title}>Нууц үг сэргээх</h2>
      <p className={classes.txt}>
        Бид таны цахим хаяг руу нууц үг сэргээх хаяг явуулах болно.
      </p>
      <label className={classes.label}>Цахим хаяг</label>
      <input
        className={classes.input}
        placeholder='name@mail.domain'
        type='email'
        value={enteredEmail}
        onChange={e => {
          setEnteredEmail(e.target.value);
        }}
      />
      <div className={classes.btnContainer}>
        <button onClick={resetPassword} className='btn'>
          БҮРТГҮҮЛЭХ
        </button>
      </div>
    </div>
  );
};

export default Forgot;
