const functions = require('firebase-functions');
const admin = require('firebase-admin');

admin.initializeApp();

const db = admin.firestore();

exports.saveToHistory = functions.firestore
  .document('urls/{id}')
  .onCreate(async snapshot => {
    const { uid, originalUrl } = snapshot.data();

    await db.doc(`users/${uid}/history/${snapshot.id}`).set({
      originalUrl,
    });
  });
