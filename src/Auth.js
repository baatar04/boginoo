import React, { useEffect, useState } from 'react';
import { auth } from './firebase';
import Loader from './pages/loader';

export const AuthContext = React.createContext();

export const AuthProvider = ({ children }) => {
  const [loading, setLoading] = useState(true);

  const [currentUser, setCurrentUser] = useState();
  const [input, setInput] = useState({
    email: '',
    password: '',
    passwordConform: '',
  });

  useEffect(() => {
    auth.onAuthStateChanged(user => {
      if (!user) {
        auth.signInAnonymously();
      }
      setCurrentUser(user);
      setLoading(false);
    });
  }, []);

  if (loading) {
    return <Loader />;
  }

  return (
    <AuthContext.Provider
      value={{
        currentUser,
        setCurrentUser,
        input,
        setInput,
        setLoading,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};
