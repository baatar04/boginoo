import classes from './input.module.css';

const Input = ({ label, placeholder, type, value, onChange, name }) => {
  return (
    <div>
      <label className={classes.label}>{label}</label>
      <input
        name={name}
        className={classes.input}
        type={type}
        placeholder={placeholder}
        value={value}
        onChange={onChange}
      ></input>
    </div>
  );
};

export default Input;
