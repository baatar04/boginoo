import React, { useState, useEffect } from 'react';
import Urls from '../components/urls';
import { useContext } from 'react';
import { AuthContext } from '../Auth';
import { firestore } from '../firebase';
import { History } from '../components/history';
import { checkIsValidUrl } from '../validators';

import classes from './home.module.css';

const Home = () => {
  const { currentUser } = useContext(AuthContext);
  const [enteredUrl, setEnteredUrl] = useState('');
  const [shortUrl, setShortUrl] = useState('');

  const handleSubmit = async event => {
    event.preventDefault();

    if (!checkIsValidUrl(enteredUrl)) {
      return alert('Invalid url');
    }

    try {
      if (currentUser) {
        const urlDoc = await firestore
          .collection(`users/${currentUser.uid}/history`)
          .add({
            originalUrl: enteredUrl,
            timestamp: new Date(),
          });
        setShortUrl(`${window.location.origin}/${urlDoc.id}`);
      }
    } catch (err) {
      console.error('Error writing document: ', err);
    }
    setEnteredUrl('');
  };

  const [histories, setHistories] = useState([]);
  useEffect(() => {
    if (currentUser) {
      firestore
        .collection(`users/${currentUser.uid}/history`)
        .orderBy('timestamp', 'desc')
        .limit(3)
        .onSnapshot(snapshot => {
          const _histories = [];
          snapshot.forEach(doc => {
            _histories.push({
              id: doc.id,
              ...doc.data(),
            });
          });

          setHistories(_histories);
        });
    }
  }, [currentUser]);

  return (
    <div className={classes.home}>
      <form className={classes.flex} onSubmit={handleSubmit}>
        <div>
          <input
            className={classes.input}
            placeholder='https://www.web-huudas.mn'
            value={enteredUrl}
            onChange={e => setEnteredUrl(e.target.value)}
          />
          <button className='btn' type='submit'>
            БОГИНОСГОХ
          </button>
        </div>
        {currentUser ? (
          <History histories={histories} />
        ) : (
          <Urls shortUrl={shortUrl} enteredUrl={enteredUrl} />
        )}
      </form>
    </div>
  );
};

export default Home;
