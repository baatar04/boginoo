import { useHistory } from 'react-router-dom';
import { useContext } from 'react';
import { AuthContext } from '../Auth';
import firebase from '../firebase';
import Input from '../components/input';

import classes from './signup.module.css';
import '../styles/vars.css';

const Signup = () => {
  const history = useHistory();
  const { currentUser, input, setInput, setCurrentUser, setLoading } =
    useContext(AuthContext);

  if (!currentUser.isAnonymous) {
    history.push('/');
  }

  const onInputChange = event => {
    const fieldName = event.target.name;

    setInput(preData => ({
      ...preData,
      [fieldName]: event.target.value,
    }));
  };

  const handleSignUp = async () => {
    if (input.password !== input.passwordConform) {
      setInput({ password: '', passwordConform: '' });
      alert('Нууц үг зөрж байна');
      return;
    }
    setLoading(true);
    try {
      const credential = firebase.auth.EmailAuthProvider.credential(
        input.email,
        input.password
      );
      const linkedUser = await currentUser.linkWithCredential(credential);
      setCurrentUser(linkedUser);
    } catch (e) {
      alert(e);
    }

    setLoading(false);
  };

  return (
    <div className={classes.register}>
      <h2 className={classes.title}>Бүртгүүлэх</h2>
      <Input
        name='email'
        onChange={onInputChange}
        value={input.email}
        label='Цахим хаяг'
        placeholder='name@mail.domain'
        type='email'
      />
      <Input
        name='password'
        onChange={onInputChange}
        value={input.password}
        label='Нууц үг'
        placeholder='password'
        type='password'
      />
      <Input
        name='passwordConform'
        onChange={onInputChange}
        value={input.passwordConform}
        label='Нууц үгээ датна уу?'
        placeholder='password'
        type='password'
      />
      <div className={classes.btnContainer}>
        <button className='btn' onClick={handleSignUp}>
          БҮРТГҮҮЛЭХ
        </button>
      </div>
    </div>
  );
};

export default Signup;
