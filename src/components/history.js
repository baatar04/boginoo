import React, { Fragment } from 'react';

import classes from './history.module.css';

export const History = ({ histories }) => {
  return (
    <Fragment>
      {histories.length > 0 && <h2 className={classes.title}>Түүх</h2>}
      {histories.map(history => {
        const shortUrl = `${window.location.origin}/${history.id}`;
        const onClipboard = () => {
          navigator.clipboard.writeText(shortUrl);
        };
        return (
          <div className={classes.container} key={history.id}>
            <div className={classes['flex-1']}>
              <p className={classes.linkTitle}>Өгөгдсөн холбоос:</p>
              <p className={classes.orignalUrl}>
                {history.originalUrl.substring(0, 20)}
              </p>
            </div>

            <div className={classes['flex-1']}>
              <p className={classes.linkTitle}>Богино холбоос:</p>
              <div className={classes.flex}>
                <p className={classes.shortUrl}>{shortUrl.substring(0, 20)}</p>
                <p className={classes.clipboard} onClick={onClipboard}>
                  Хуулж авах
                </p>
              </div>
            </div>
          </div>
        );
      })}
    </Fragment>
  );
};
