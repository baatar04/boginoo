import { useHistory } from 'react-router';
import { useContext } from 'react';
import { AuthContext } from '../Auth';
import { auth } from '../firebase';

import classes from './header.module.css';

const Header = () => {
  const history = useHistory();
  const { currentUser, setLoading } = useContext(AuthContext);

  const pushLogin = () => {
    history.push('/login');
  };

  const signOut = () => {
    auth.signOut();
    setLoading(true);
  };

  return (
    <div className={classes.header}>
      <p className={classes.txt}>ХЭРХЭН АЖИЛЛАДАГ ВЭ?</p>
      {currentUser?.isAnonymous ? (
        <button className='btn' onClick={pushLogin}>
          НЭВТРЭХ
        </button>
      ) : (
        <button className='btn' onClick={signOut}>
          ГАРАХ
        </button>
      )}
    </div>
  );
};

export default Header;
