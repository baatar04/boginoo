import { useContext } from 'react';
import { AuthContext } from '../Auth';
import { useHistory } from 'react-router-dom';
import Input from '../components/input';

import classes from './login.module.css';
import { auth } from '../firebase';

const Login = () => {
  const { currentUser, input, setInput } = useContext(AuthContext);
  const history = useHistory();

  if (!currentUser.isAnonymous) {
    history.push('/');
  }

  const onInputChange = event => {
    const fieldName = event.target.name;

    setInput(preData => ({
      ...preData,
      [fieldName]: event.target.value,
    }));
  };

  const login = async () => {
    try {
      await auth.signInWithEmailAndPassword(input.email, input.password);
      console.log('logged in');
      setInput('');
      history.push('/');
    } catch (err) {
      alert(err);
    }
  };

  return (
    <div className={classes.login}>
      <h2 className={classes.title}>Нэвтрэх</h2>
      <Input
        name='email'
        onChange={onInputChange}
        value={input.email}
        label='Цахим хаяг'
        placeholder='name@mail.domain'
        type='email'
      />
      <Input
        name='password'
        onChange={onInputChange}
        value={input.password}
        label='Нууц үг'
        placeholder='password'
        type='password'
      />
      <div className={classes.cechboxContainer}>
        <label className={classes.checkbox}>
          <input type='checkbox' />
          Намайг сана
        </label>
        <p
          className={classes.forgotPassword}
          onClick={() => {
            history.push('/forgot');
          }}
        >
          Нууц үгээ мартсан
        </p>
      </div>
      <div className={classes.btnContainer}>
        <button onClick={login} className='btn'>
          НЭВТРЭХ
        </button>
        <p
          onClick={() => {
            history.push('/signup');
          }}
          className={classes.register}
        >
          Шинэ хэрэглэгч бол энд дарна уу?
        </p>
      </div>
    </div>
  );
};

export default Login;
