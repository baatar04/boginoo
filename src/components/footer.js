import classes from './footer.module.css';

const Footer = () => {
  return (
    <div className={classes.footer}>
      <h5 className={classes.txt}>Made with ❤️ by Nest Academy</h5>
      <p className={classes.io}>@boginoo.io 2021</p>
    </div>
  );
};

export default Footer;
