import React from 'react';
import { ReactComponent as Logo } from '../assets/loader.svg';

import classes from './loader.module.css';

const Loader = () => {
  return (
    <div className={classes.center}>
      <Logo />
    </div>
  );
};

export default Loader;
