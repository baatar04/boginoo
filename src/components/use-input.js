import { useState } from 'react';

export const useInput = (initialValue) => {
  const [value, setValue] = useState(initialValue);

  const resetValue = () => setValue(initialValue);

  const bind = {
    value,
    onChange: (event) => setValue(event.target.value),
  };
  return [value, bind, resetValue];
};
