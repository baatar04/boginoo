import React, { useEffect, useState, useContext } from 'react';
import { useParams } from 'react-router-dom';

import { firestore } from './firebase';
import { AuthContext } from './Auth';

const Urlredirect = () => {
  const { currentUser } = useContext(AuthContext);
  const { firestoreId } = useParams();
  const [errorMassage, setErrorMessage] = useState();

  useEffect(() => {
    const getUrl = async () => {
      const doc = await firestore
        .doc(`users/${currentUser.uid}/history/${firestoreId}`)
        .get();

      if (doc.exists) {
        window.location.href = doc.data().originalUrl;
      } else {
        if (firestoreId === undefined) setErrorMessage('404');
      }
    };
    getUrl();
  }, [firestoreId, currentUser]);
  if (setErrorMessage) {
    return <h1>{errorMassage}</h1>;
  }
  return <h1>Please wait...</h1>;
};

export default Urlredirect;
